#!/bin/env/python3
import os
from os import listdir
from os.path import isfile

def mostrarMsj(strMensaje=""):
    """
    Muestra el mensaje, string, que se le pase o de manera predeterminada
    una linea vacia.
    Se espera que el usuario presione cualquier tecla para continuar
    """
    input("{}\nPresione cualquier tecla para continuar".format(strMensaje))

def limpiarPantalla():
    os.system('clear')

def listarArchivos(path=""):
    """
    Retorna una lista con los archivos en la ubicacion actual(default) a menos
    que se le pase un path
    """
    if bool(path):
        l = listdir(path)
    else:
        l = listdir()

    return l

def soloArchivos(listaArchivos):
    """
    Recibe una lista con archivos(incluidos directorios),
    retorna una lista sin los directorios
    """
    l = []
    for archivo in listaArchivos:
        if isfile(archivo):
            l.append(archivo)

    return l

def encontrarArchivos(listaArchivos, patron):
    """
    Dada una lista de archivos y un patron, se filtran los archivos por el patron dado.
    retorna una lista de archivos que contengan el patron dado
    """
    l = []
    for archivo in listaArchivos:
        if patron in archivo:
            l.append(archivo)
    return l


def menu(listaOpciones):
    '''
    Recibe una lista con las opciones disponibles para el menu.
    Asigna un numero natural a cada item en la lista
    retorna la opcion seleccionada como un entero (natural)
    '''
    for elemento in range(len(listaOpciones)):
        indice = elemento + 1
        print("{}. {}".format(indice, listaOpciones[elemento]))

    """
    si la opcion seleccionada no es la adecuada se devuelve el valor -1
    puede considerarse como error
    """
    try:
        opcion = int(input(">? "))
    except:
        opcion = -1
    return opcion
