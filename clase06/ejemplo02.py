#!/bin/env python3

from flask import Flask,render_template
import funcion as f

app = Flask (__name__)

@app.route('/')
def hello():
    salida = f.crearArchivo()
    msj = "hola \n" + salida
    return render_template("index.html", msj=msj)

if __name__ == '__main__':
    app.run()
