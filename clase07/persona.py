#!/bin/env python3

class Person:
    def __init__(self, name, age):
        self.name = name
        self.age = age

p1 = Person("Jhon", 23)
print(p1.name)
print(p1.age)

del p1
