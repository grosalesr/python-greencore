# argumento numeros es una lista
def suma(numeros):
    n = 0
    for numero in numeros:
        n += numero
    return n

def resta(numeros):
    n = 0
    for numero in numeros:
        n -= numero
    return n

def multiplicar(numeros):
    n = 1
    for numero in numeros:
        n *= numero
    return n

def dividir(dividendo, divisor):
    if divisor == 0:
        return "Division entre cero - Operacion no permitida"
    else:
        return dividendo/divisor
