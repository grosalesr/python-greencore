
class Nose:
    def __iter__(self):
        self.a = 1
        return self

    def __next__(self):
        x = self.a
        self.a += 1
        return 1

myclass = Nose()
myiter = iter(myclass)
print(next(myiter))
