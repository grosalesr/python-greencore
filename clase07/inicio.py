#!/bin/env python3

# Documentation:
# https://jinja.palletsprojects.com/en/2.10.x/templates/
from jinja2 import Environment
from jinja2 import FileSystemLoader

def guardarArchivo(nombre, datos):
    # De manera predeterminada se van a sobreescribir los archivos
    script = open(nombre, "w")
    script.write(datos)
    script.close()

# Define el directorio de templates
templates = Environment(loader=FileSystemLoader('templates'),
            trim_blocks=True)

# Despliega el menu para la captura del nombre del objeto
restriccionesObjeto = ["Debe ser una sola palabra",
                       "Debe empezar con mayuscula",
                       "No debe contener numeros"]

print("\t\tRestricciones Nombre\n")
for restriccion in restriccionesObjeto:
    print("- {}".format(restriccion))

print()
nombreObjeto = input("Ingrese el nombre del objeto: ")

# Carga los templates de la aplicacion
app = templates.get_template('app.py.j2')
objeto = templates.get_template('objeto.py.j2')

# Reemplaza los placeholders de cada template por los valores asignados
# nombreObjeto (variable jinja en los templates) = nombreObjeto (nombre ingresado por el usuario)
appPy = app.render(nombreObjeto=nombreObjeto)
objetoPy = objeto.render(nombreObjeto=nombreObjeto)

# Asocia los nombres de los archivos que componen la aplicacion con los templates
# ya renderizados
d = {"app.py":appPy,
     "objeto.py":objetoPy}
for k, v in d.items():
    guardarArchivo(k, v)
