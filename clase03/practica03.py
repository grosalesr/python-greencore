#!/bin/env/python3
'''
Cree un software que le solicite al usuario el nombre del estudiante y lo guarde en una base de datos no relacional.
Al final le muestre al usuario la lista de todos los estudiantes.
'''
import utilidades as util

contador = 0
extension = "txt"
nombreArchivo = "nombreEstudiante"
opcionesMenu = ["Ingresar Datos","Mostrar Datos","Salir"]

generarNombre = lambda indice: "{}-{}.{}".format(nombreArchivo, indice, extension)

# Ajusta el contador
archivosEnDirectorio = util.listarArchivos()
contador = len(util.encontrarArchivos(archivosEnDirectorio, nombreArchivo))

salir = False
while (not salir):
    util.limpiarPantalla()
    opcion = util.menu(opcionesMenu)

    if opcion == 1:
        nombre = input("Nombre Estudiante: ")
        archivo = generarNombre(contador)
        """
        El modo es escritura, si algun archivo ya existe y contiene informacion
        esta sera sobreescrita. Para evitar este comportamiento, usar el modo a (append)
        """
        datos = open(archivo, "w")
        datos.write(nombre)
        datos.close()
        contador += 1

    elif opcion == 2:
        try:
            for indice in range(contador):
                archivo = generarNombre(indice)
                datos = open(archivo, "r")
                for linea in datos:
                    print(linea)
                datos.close()
            util.mostrarMsj()
        except FileNotFoundError:
            strMsj = ("No se encontro el/los archivo(s)")
            util.mostrarMsj(strMsj)

    elif opcion == 3:
        salir = True
        break

