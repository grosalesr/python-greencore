#!/bin/env/python3

import sqlite3

conexion = sqlite3.connect("bd.db")
try:
    conexion.execute("""CREATE TABLE articulos(
            codigo integer primary key AUTOINCREMENT,
            descripcion text,
            precio real)""")

    conexion.execute("insert into articulos (descipcion,precio) values(?.?)", ("naranja", 20))
    conexion.commit()

    curso = conexion.execute('select codigo,descripcion,precio from articulos')
    for fila in curso:
        print(fila)

    print("Se creo la tabla")
except sqlite3.OperationalError:
    print("ya existe")
conexion.close()
