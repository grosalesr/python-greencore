#!/bin/env/python3
import os.path
import sqlite3
import utilidades as util


opcionesMenu = ["Ingresar Datos","Mostrar Datos","Salir"]

salir = False
dbName = "practica1.db"
conexion = sqlite3.connect(dbName)

if (os.path.isfile(dbName)):
    try:
        conexion.execute("""CREATE TABLE articulos(
                        codigo integer primary key AUTOINCREMENT,
                        descripcion text,
                        precio real)""")

    except:
        strMsj = "DB existe, conexion establecida"
        util.mostrarMsj(strMsj)


while (not salir):
    util.limpiarPantalla()
    opcion = util.menu(opcionesMenu)

    if opcion == 1:
        descripcion = input("Descripcion: ")
        try:
            precio = int(input("Precio: "))
            conexion.execute("insert into articulos (descripcion, precio) values (?, ?)", (descripcion, precio))
            conexion.commit()
        except sqlite3.OperationalError:
            strMsj = "Error ingresando datos"
            util.mostrarMsj(strMsj)
        except ValueError:
            strMsj = "El valor ingresado no es un numero"
            util.mostrarMsj(strMsj)

    elif opcion == 2:
        curso = conexion.execute('select codigo,descripcion,precio from articulos')
        for fila in curso:
            print(fila)
        util.mostrarMsj()

    elif opcion == 3:
        salir = True
        break
    else:
        strMsj = "Opcion incorrecta"
        util.mostrarMsj(strMsj)


conexion.close()
