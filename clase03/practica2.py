#!/bin/env/python3
'''
Cree un software con dos bases de datos, la primera con el nombre "empleados" y la segunda "transacciones".
En la base de datos empleados, cree dos tables con los nombres:
    1) Empleados
    2) Horarios

En la tabla empleados cree 3 casillas:
    1) Nombre
    2) Cedula
    3) Puesto

En la base de datos transacciones, cree 3 tablas:
    1) Inventario
    2) Efectivo
    3) Activos
con las casillas en Inventario:
    1) Entrada
    2) Salida
    3) Saldo

Solicitele al usuario ingresar los datos de un nuevo empleado o ver los empleados que ya hay
'''

import os.path
import sqlite3
import utilidades as util


salir = False
opcionesMenu = ["Ingresar Datos","Mostrar Datos","Salir"]

#Declaracion de bases de datos
empleadosDB = "empleados.db"
transaccionesDB = "transacciones.db"

#Establece conexion con las bases de datos
empleados = sqlite3.connect(empleadosDB)
transacciones = sqlite3.connect(transaccionesDB)

if (os.path.isfile(empleadosDB) or os.path.isfile(transaccionesDB)):
    try:
        # La cedula funciona como identificador unico
        empleados.execute("""CREATE TABLE IF NOT EXISTS Empleados(
                        cedula integer primary key,
                        nombre TEXT,
                        puesto TEXT)""")

        empleados.execute("""CREATE TABLE IF NOT EXISTS Horarios""")


        transacciones.execute("""CREATE TABLE IF NOT EXISTS Inventario(
                        saldo integer primary key AUTOINCREMENT,
                        Entrada text,
                        Salida text)""")

        transacciones.execute("""CREATE TABLE IF NOT EXISTS Efectivo""")
        transacciones.execute("""CREATE TABLE IF NOT EXISTS Activos""")


    except:
        strMsj = "DB existe, conexion establecida"
        util.mostrarMsj(strMsj)


while (not salir):
    util.limpiarPantalla()
    opcion = util.menu(opcionesMenu)

    if opcion == 1:
        nombre = input("Nombre: ")
        puesto = input("Puesto: ")
        try:
            cedula = int(input("cedula: "))
            empleados.execute("INSERT INTO Empleados (cedula, nombre, puesto) values (?, ?, ?)", (cedula, nombre, puesto))
            empleados.commit()
        except sqlite3.OperationalError:
            strMsj = "Error ingresando datos"
            util.mostrarMsj(strMsj)
        except ValueError:
            strMsj = "El valor ingresado no es un numero"
            util.mostrarMsj(strMsj)

    elif opcion == 2:
        infoEmpleados = empleados.execute('select cedula, nombre, puesto FROM Empleados')
        for empleado in infoEmpleados:
            print(empleado)
        util.mostrarMsj()

    elif opcion == 3:
        salir = True
        break
    else:
        strMsj = "Opcion incorrecta"
        util.mostrarMsj(strMsj)


empleados.close()
transacciones.close()
