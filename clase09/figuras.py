#!/usr/env python3

class Punto:
    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y
    def __repr__(self):
        return "x: {}, y: {}".format(self.x, self.y)

# Se agrega la clase circulo que herede de Punto

class Circulo(Punto):
    def __init__(self, radio, *args, **kwargs):
        self.radio = radio
        super().__init__(*args, **kwargs)
    def __repr__(self):
        return "x: {}, y: {}, radio: {}".format(self.x, self.y, self,radio)
