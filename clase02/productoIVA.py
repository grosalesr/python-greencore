#!/usr/env/python3
import os

limpiarPantalla = lambda: os.system('clear')

IVA = 0.13
while True:
    limpiarPantalla()
    opcion = int(input("Que desea hacer \n  1- Calcular IVA\n  2- Salir\n>? "))
    if opcion == 1:
        producto = input("Producto: ")
        try:
            precio = float(input("Precio: "))
            IVAPrecio = precio * IVA
            total = precio + IVAPrecio
            print("\n\nProducto: {}\nIVA: {}\nTotal:{}".format(producto, IVAPrecio, total))
        except:
            print("El precio indicado no es correcto, intente de nuevo")
        input("presione cualquier tecla para continuar...")
    elif opcion == 2:
        break
