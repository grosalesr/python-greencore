#!/bin/env/python3

import sqlite3

conn = sqlite3.connect("myDB.db")
c = conn.cursor()
try:
    c.execute("CREATE TABLE user (name text, age integer)")
    c.execute("INSERT INTO user VALUES ('juan', 42)")
    conn.commit()
except:
    print("tabla ya existe, mostrando datos\n")

c.execute("SELECT * FROM user")
print(c.fetchall())
conn.close()
