#!/usr/env/python3
import os
from modulosProfesor import metodos

limpiarPantalla = lambda: os.system('clear')

estudiantes = []
while True:
    limpiarPantalla()
    opcion = metodos.mostrarMenu()

    if opcion == 1:
        estudiantes = metodos.agregar(estudiantes)
    elif opcion == 2:
        metodos.mostrar(estudiantes)
        input("Presione cualquier tecla para volver al menu")
    elif opcion == 3:
        metodos.eliminar(estudiantes)
    elif opcion == 4:
        break

