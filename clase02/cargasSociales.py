#!/usr/env/python3
import os

limpiarPantalla = lambda: os.system('clear')

# Cargas sociales
porcentajePatrono = 0.3 #30%
porcentajeTrabajador = 0.1 #10%

while True:
    limpiarPantalla()
    opcion = int(input("Que desea hacer \n  1- Calcular Cargas Sociales\n  2- Salir\n>? "))
    if opcion == 1:
        try:
            salarioBruto = float(input("Salario Bruto: "))
            totalPatrono = salarioBruto * porcentajePatrono
            totaltrabajador = salarioBruto * porcentajeTrabajador
            print("\n\nDesglose Salario\n----------\nPorcentajes\n")
            print("Patrono: {}\nTrabajador: {}".format(totalPatrono, totaltrabajador))
            break
        except:
            print("El salario bruto indicado no es correcto, intente de nuevo")
        input("presione cualquier tecla para continuar...")
    elif opcion == 2:
        break
