#!/usr/env/python3

def mostrarMenu():
    opcion = int(input('''
Que desea hacer
1-Agregar Estudiantes
2-Mostrar Estudiantes
3-Eliminar
4-Salir
>? '''))
    return opcion

# lista es el argumento de los siguiente parametro
def agregar(estudiantes):
    estudiante = input("Estudiante: ")
    estudiantes.append(estudiante)
    return estudiantes

# recibe una lista
def mostrar(estudiantes):
    for estudiante in range(len(estudiantes)):
        print("Estudiante{}: {}".format(estudiante, estudiantes[estudiante]))

def eliminar(estudiantes):
    estudiante = input("Estudiante a eliminar: ")
    estudiantes.remove(estudiante)
    return estudiantes

