#!/usr/env/python3
import os

limpiarPantalla = lambda: os.system('clear')

estudiantes = []
while True:
    limpiarPantalla()
    opcion = int(input("Que desea hacer \n  1- Agregar\n  2- Salir\n>? "))
    if opcion == 1:
        estudiante = input("Estudiante: ")
        estudiantes.append(estudiante)
    elif opcion == 2:
        break

for estudiante in range(len(estudiantes)):
    print("Estudiante{}: {}".format(estudiante, estudiantes[estudiante]))
