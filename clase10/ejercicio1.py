#!/bin/env python3

from mechanize import Browser
import csv

with open ('namecsv.csv') as csvfile:
    reader = csv.DictReader(csvfile)
    for rows in csvfile:
        browser = Browser()
        browser.open("https://www.consulta.tse.go.cr/consulta_persona/consulta_cedula.aspx")
        form = browser.select_form("form1")
        browser["txtcedula"]=rows
        response=browser.submit()
        name=str(rows).replace('\n', "")
        print(name)
        filename = name + ".html"
        file=open(filename,"w")
        file.write(response.read().decode("utf-8"))
        file.close()
